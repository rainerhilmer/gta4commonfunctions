﻿using Cyron43.GtaIV.Common;

namespace GTA4CommonFunctions.Tests
{
   public class ConfigurationContainer : IConfigurationContainer
   {
      public string SomeText { get; set; }
      public int Version { get; set; }
   }
}