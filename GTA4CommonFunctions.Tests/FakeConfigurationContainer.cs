﻿using Cyron43.GtaIV.Common;

namespace GTA4CommonFunctions.Tests
{
   /// <summary>
   /// Do not write a configuration file for this container. It's purpose is
   /// to test what happens if no such config file can be found.
   /// </summary>
   public class FakeConfigurationContainer : IConfigurationContainer
   {
      public int Version { get; set; }
   }
}