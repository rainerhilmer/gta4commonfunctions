﻿using System.Windows.Forms;

namespace Autodrive
{
   public class KeyContainer
   {
      public bool Alt { get; set; }

      public bool Ctrl { get; set; }

      public Keys Key { get; set; }

      public string Name { get; set; }

      public bool Shift { get; set; }
   }
}