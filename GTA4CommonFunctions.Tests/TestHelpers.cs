﻿using System.Collections.Generic;
using Cyron43.GtaIV.Common.FakesForTests;

namespace GTA4CommonFunctions.Tests
{
   internal static class TestHelpers
   {
      #region vectorcases

      public static readonly List<VectorTestCase> VectorTestCases = new List<VectorTestCase>
                                                               {
                                                                  new VectorTestCase
                                                                  {
                                                                     Vector = new Vector3(0, 2, 0),
                                                                     ExpectedDegrees = 0
                                                                  },
                                                                  new VectorTestCase
                                                                  {
                                                                     Vector = new Vector3(-2, 2, 0),
                                                                     ExpectedDegrees = 45
                                                                  },
                                                                  new VectorTestCase
                                                                  {
                                                                     Vector = new Vector3(-2, 0, 0),
                                                                     ExpectedDegrees = 90
                                                                  },
                                                                  new VectorTestCase
                                                                  {
                                                                     Vector = new Vector3(-2, -2, 0),
                                                                     ExpectedDegrees = 135
                                                                  },
                                                                  new VectorTestCase
                                                                  {
                                                                     Vector = new Vector3(0, -2, 0),
                                                                     ExpectedDegrees = 180
                                                                  },
                                                                  new VectorTestCase
                                                                  {
                                                                     Vector = new Vector3(2, -2, 0),
                                                                     ExpectedDegrees = 225
                                                                  },
                                                                  new VectorTestCase
                                                                  {
                                                                     Vector = new Vector3(2, 0, 0),
                                                                     ExpectedDegrees = 270
                                                                  },
                                                                  new VectorTestCase
                                                                  {
                                                                     Vector = new Vector3(2, 2, 0),
                                                                     ExpectedDegrees = 315
                                                                  },
                                                               };

      #endregion
   }
}
