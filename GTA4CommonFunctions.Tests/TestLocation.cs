﻿using Cyron43.GtaIV.Common;

namespace GTA4CommonFunctions.Tests
{
   public class TestLocation : ILocation
   {
      public int Id { get; set; }
      public float X { get; set; }
      public float Y { get; set; }
      public float Z { get; set; }
      public string TargetName { get; set; }
   }
}
