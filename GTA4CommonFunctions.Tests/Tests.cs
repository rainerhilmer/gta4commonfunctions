﻿using System.Reflection;
using Cyron43.GtaIV.Common;
using FakeItEasy;
using FluentAssertions;
using NUnit.Framework;
using Ploeh.AutoFixture;
using SomeTestAssembly;

namespace GTA4CommonFunctions.Tests
{
   [TestFixture]
   public class Tests
   {
      // ReSharper disable InconsistentNaming
      private const string _fullPath = @"a:\GTAIVCommonFunctionsTestConfig.xml";
      private const string _otherFullPath = @"a:\SomeTestAssemblyConfig.xml";
      private const int _someNumberForOtherAssembly = 3;
      // ReSharper restore InconsistentNaming

      #region assembly identities

      private readonly IModIdentity _nonExistentAssemblyData =
         A.Fake<IModIdentity>();

      private readonly IModIdentity _someTestAssemblyData =
         new SomeTestAssembly.ModIdentity
         {
            AssemblyName = "SomeTestAssembly",
            FullPath = _otherFullPath,
            Version = 2
         };

      private readonly IModIdentity _thisAssemblyData =
         new ModIdentity
         {
            AssemblyName = Assembly.GetExecutingAssembly().GetName().Name,
            FullPath = _fullPath,
            Version = 1
         };

      #endregion assembly identities

      [TestFixtureSetUp]
      public void Setup()
      {
         var config = new ConfigurationContainer
         {
            SomeText = "Bla",
            Version = 1
         };

         ConfigurationProvider.SaveConfig(config, _fullPath);

         var otherConfig = new SomeTestAssembly.ConfigurationContainer
         {
            SomeNumber = _someNumberForOtherAssembly,
            Version = 2
         };

         ConfigurationProvider.SaveConfig(otherConfig, _otherFullPath);
         A.CallTo(() => _nonExistentAssemblyData.AssemblyName).Returns("NonExistentAssembly");
         A.CallTo(() => _nonExistentAssemblyData.FullPath).Returns(@"a:\NonExistent.xml");
         A.CallTo(() => _nonExistentAssemblyData.Version).Returns(0);
      }

      [Test]
      public void ConfigurationContainer_is_null_because_file_not_found()
      {
         ConfigurationProvider.ClearDictionary();
         var sut = ConfigurationProvider.CreateOrGetThisFor(_nonExistentAssemblyData);
         ErrorType typeOfError;
         var config = sut.GetConfigurationFor<FakeConfigurationContainer>(
            _nonExistentAssemblyData, out typeOfError, supressInGameMessage: true);
         config.Should().BeNull("Config should be null.");
         typeOfError.Should().Be(ErrorType.FileNotFound, "File should not be found.");
      }

      [Test]
      public void ConfigurationContainer_is_null_because_the_wrong_config_file_is_called()
      {
         ConfigurationProvider.ClearDictionary();
         var sut = ConfigurationProvider.CreateOrGetThisFor(_thisAssemblyData);
         ErrorType typeOfError;
         var config = sut.GetConfigurationFor<SomeTestAssembly.ConfigurationContainer>(
            _thisAssemblyData, out typeOfError, supressInGameMessage: true);
         config.Should().BeNull("Config should be null.");
         typeOfError.Should().Be(ErrorType.SerializationException, "Wrong file was called");
      }

      [Test]
      public void Gets_namespace_name()
      {
         /* Ich habe den Assemblynamen extra anders gewählt,
          * weil in den Mods auch immer .net angehängt ist. */
         var result = typeof(SomeTestAssembly.ModIdentity).Namespace;
         result.Should().Be("SomeTestAssembly");
      }

      [Test]
      public void Other_assembly_gets_its_config()
      {
         ConfigurationProvider.ClearDictionary();
         var sut = new SomeClass();
         sut.ReadMyConfig();
         sut.Configuration.SomeNumber.Should().Be(_someNumberForOtherAssembly);
      }

      [Test]
      public void Reads_autodrive_config()
      {
         ConfigurationProvider.ClearDictionary();
         var autodriveIdentity = new ModIdentity
                                 {
                                    AssemblyName = Autodrive.ModIdentityProvider.Identity.AssemblyName,
                                    FullPath = Autodrive.ModIdentityProvider.Identity.FullPath,
                                    Version = Autodrive.ModIdentityProvider.Identity.Version
                                 };
         ErrorType typeOfError;
         var config = ConfigurationProvider.CreateOrGetThisFor(autodriveIdentity)
            .GetConfigurationFor<Autodrive.ConfigurationContainer>(
               autodriveIdentity,
               out typeOfError,
               supressInGameMessage: true);
         config.Should().NotBeNull();
         config.AlwaysDriveYourself.Should().BeTrue();
         typeOfError.Should().Be(ErrorType.None);
         config.Keys.Count.Should().BeGreaterThan(0);
      }

      [Test]
      public void Reads_the_config()
      {
         ConfigurationProvider.ClearDictionary();
         var sut = ConfigurationProvider.CreateOrGetThisFor(_thisAssemblyData);

         #region Create some interference...

         ConfigurationProvider.CreateOrGetThisFor(_someTestAssemblyData);

         #endregion Create some interference...

         ErrorType typeOfError;
         var config = sut.GetConfigurationFor<ConfigurationContainer>(
            _thisAssemblyData, out typeOfError, supressInGameMessage: true);
         config.Should().NotBeNull();
         config.SomeText.Should().Be("Bla");
         typeOfError.Should().Be(ErrorType.None, "ErrorType should be None.");
      }

      /// <summary>
      /// This test just demonstrates that the new centralized ILocation
      /// interface does not change the tag names of a location file.
      /// </summary>
      [Test, Explicit]
      public void Saves_location()
      {
         var fixture = new Fixture();
         var location = new TestLocation
                        {
                           Id = fixture.Create<int>(),
                           TargetName = fixture.Create<string>(),
                           X = fixture.Create<float>(),
                           Y = fixture.Create<float>(),
                           Z = fixture.Create<float>()
                        };
         new XmlIo(@"a:\TestLocation.xml").Save(location);
      }
      
      [Test]
      public void TypeOfError_is_keyNotFound_because_assembly_has_not_registered()
      {
         ConfigurationProvider.ClearDictionary();
         var sut = ConfigurationProvider.CreateOrGetThisFor(_someTestAssemblyData);
         ErrorType typeOfError;
         sut.GetConfigurationFor<ConfigurationContainer>(
            _nonExistentAssemblyData, out typeOfError, supressInGameMessage: true);
         typeOfError.Should().Be(ErrorType.KeyNotFound);
      }

      [Test]
      public void Version_is_valid()
      {
         ConfigurationProvider.ClearDictionary();
         var configProvider = ConfigurationProvider
            .CreateOrGetThisFor(_someTestAssemblyData);
         // We don't need the config here but just the version number
         // verification.
         ErrorType typeOfError;
         configProvider.GetConfigurationFor<SomeTestAssembly.ConfigurationContainer>(
            _someTestAssemblyData, out typeOfError, supressInGameMessage: true);
         typeOfError.Should().Be(ErrorType.None);
      }
   }
}