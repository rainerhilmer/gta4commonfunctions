﻿using Cyron43.GtaIV.Common.FakesForTests;

namespace GTA4CommonFunctions.Tests
{
   public class VectorTestCase
   {
      public Vector3 Vector { get; set; }
      public int ExpectedDegrees { get; set; }
   }
}
