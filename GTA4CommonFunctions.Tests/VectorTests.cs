﻿using System.Collections.Generic;
using Cyron43.GtaIV.Common.FakesForTests;
using FluentAssertions;
using NUnit.Framework;

//note: Vor den Testläufen auf Debug any CPU umstellen.

namespace GTA4CommonFunctions.Tests
{
   [TestFixture]
   public class VectorTests
   {
      private CommonVectorFunctions _commonVectorFunctions;

      [TestFixtureSetUp]
      public void Setup()
      {
         _commonVectorFunctions = new CommonVectorFunctions();
      }
      
      [Test]
      public void LineToHeading_returns_correct_value()
      {
         var playerCharacterPosition = new Vector3(0, 0, 0);
         var correctPedPosition = new Vector3(0, 1.5f, 0);
         var angleToCorrectPed = _commonVectorFunctions.LineToHeading(playerCharacterPosition, correctPedPosition);
         angleToCorrectPed.Should().Be(0);
      }

      [Test]
      public void DirectionToHeading_returns_correct_value()
      {
         var direction = new Vector3(0, -1, 0); // showing to north
         var headingFromDirection = _commonVectorFunctions.DirectionToHeading(direction);
         headingFromDirection.Should().Be(0);
      }

      /// <summary>
      /// Dieser Test ist außerdem ein Anwendungsdemo.
      /// </summary>
      [Test]
      public void Gets_the_ped_in_front()
      {
         var playerCharacterPosition = new Vector3(0, 0, 0);
         var direction = new Vector3(0, -1, 0); // showing to north
         const float distance = 2.0f;
         var correctPedPosition = new Vector3(0, 1.5f, 0);
         var wrongPedPosition = new Vector3(-1.5f, 0, 0);
         var pedsAround = new List<Vector3> {correctPedPosition, wrongPedPosition};
         var tempList = new List<Vector3>(pedsAround);
         var area = new ScanArea
                    {
                       GeneralScanAreaDirection = direction,
                       MaximumDistance = distance,
                       HeadingTolerance = 20,
                       StartPosition = playerCharacterPosition
                    };
         foreach(var pedPosition in tempList)
         {
            if(!_commonVectorFunctions.TargetPositionIsWithinArea(pedPosition, area))
               pedsAround.Remove(pedPosition);
         }
         pedsAround.Count.Should().Be(1, "one ped is in the desired area");
         pedsAround[0].Should().Be(correctPedPosition, "this ped should be the one");
      }

      [Test]
      [TestCaseSource(typeof(TestHelpers), "VectorTestCases")]
      public void Test_coordToDegree(VectorTestCase vectorTestCase)
      {
         var v1 = new Vector3(0, 0, 0);
         var v2 = vectorTestCase.Vector;
         _commonVectorFunctions.LineToHeading(v1, v2).Should().Be(vectorTestCase.ExpectedDegrees);
      }
   }
}