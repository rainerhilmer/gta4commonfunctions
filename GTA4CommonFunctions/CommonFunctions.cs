﻿using System;
using System.Linq;
using System.Security.Cryptography;
using GTA;
using GTA.Native;

namespace Cyron43.GtaIV.Common
{
   /// <summary>
   /// Provides a set of functions commonly used by my mods.
   /// </summary>
   /// <remarks>The main purpose of this class is to reduce memory
   /// consumption through a centralized functionality source.
   /// All properties and methods are thread safe.</remarks>
   public static class CommonFunctions
   {
      /// <summary>
      /// A long tick interval constant of 1500ms.
      /// </summary>
      public const int LONG_INTERVAL = 1500;

      /// <summary>
      /// A minumum altitude constant of 100ft.
      /// </summary>
      public const short MIN_ALT_FIX = 100;

      /// <summary>
      /// A short interval tick interval constant of 250ms.
      /// </summary>
      public const int SHORT_INTERVAL = 250;

      private const string SCRIPT_ENGINE_FAILURE_MESSAGE =
         "~r~Script engine failure! Please use reloadscripts on the"
         + " console window or reload the game.~w~";

      private static readonly object SyncLock = new object();
      private static RNGCryptoServiceProvider rnd;

      /// <summary>
      /// Gets the file repository path which is Documents\GTA IV\ModSettings.
      /// </summary>
      /// <value>{CurrentUserAccountName}\Documents\GTA IV\ModSettings\</value>
      public static string FileRepositoryPath
      {
         get
         {
            lock(SyncLock)
            {
               return Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)
                      + @"\GTA IV\ModSettings\";
            }
         }
      }

      /// <summary>
      /// Unbelievable but true, <c>World.CurrentDate.DayOfWeek</c> returns
      /// the wrong day!
      /// </summary>
      public static DayOfWeek CorrectedCurrentDay(DayOfWeek dayOfWeek)
      {
         lock(SyncLock)
         {
            var dayNumber = (int) dayOfWeek;
            dayNumber += 4;
            if(dayNumber > 6)
               dayNumber -= 7;
            return (DayOfWeek) dayNumber;
         }
      }

      /// <summary>
      /// The vehicle speed returned by the .net Scripthook is always twice
      /// as high as the real speed.
      /// </summary>
      public static float CorrectedSpeed(float speed)
      {
         lock(SyncLock)
         {
            return speed / 2.0f;
         }
      }

      /// <summary>
      /// Deletes a ped with his vehicle when off screen and outside of a
      /// given <paramref name="perimeter"/>.
      /// </summary>
      /// <param name="vehicle">The vehicle.</param>
      /// <param name="playerPosition">The player position.</param>
      /// <param name="ped">The ped.</param>
      /// <param name="perimeter">The perimeter.</param>
      /// <returns>
      /// <c>false</c> if either the vehicle does not exist or the vehicle is
      /// still on screen or the vehicle is inside the perimeter, otherwise
      /// <c>true</c>.
      /// </returns>
      public static bool DeleteWhenOffScreen(Vehicle vehicle, Vector3 playerPosition, Ped ped, float perimeter)
      {
         lock(SyncLock)
         {
            if(!VehicleExists(vehicle) || vehicle.isOnScreen ||
               (vehicle.Position.DistanceTo(playerPosition) <= perimeter))
               return false;
            if(PedExists(ped))
               ped.Delete();
            vehicle.isRequiredForMission = false;
            vehicle.Delete();
            return true;
         }
      }

      /// <summary>
      /// Deletes a vehicle when off screen and outside of a given <paramref
      /// name="perimeter"/>.
      /// </summary>
      /// <param name="vehicle">The vehicle.</param>
      /// <param name="playerPosition">The player position.</param>
      /// <param name="perimeter">The perimeter.</param>
      /// <returns>
      /// <c>false</c> if either the vehicle does not exist or the vehicle is
      /// still on screen or the vehicle is inside the perimeter, otherwise
      /// <c>true</c>.
      /// </returns>
      public static bool DeleteWhenOffScreen(Vehicle vehicle, Vector3 playerPosition, float perimeter)
      {
         return DeleteWhenOffScreen(vehicle, playerPosition, null, perimeter);
      }

      /// <summary>
      /// Displays a given <paramref name="text"/> for a given <paramref
      /// name="duration"/> at the bottom of the screen.
      /// </summary>
      /// <param name="text">The text.</param>
      /// <param name="duration">The duration.</param>
      public static void DisplayText(string text, int duration)
      {
         lock(SyncLock)
         {
            Function.Call("PRINT_STRING_WITH_LITERAL_STRING_NOW",
               new Parameter[] {"STRING", text, duration, 1});
         }
      }

      /// <summary>
      /// Ersatz für die FadeScreenIn-Methode im .net script hook.
      /// </summary>
      /// <remarks>
      /// Leider kann keine Duration angegeben werden, weil es sonst zu einer
      /// Exception kommt (irgend etwas mit remote event nd Domain), darum
      /// dieser Ersatz. Ein einfacher workaround ist, vor dem Aufruf der
      /// Methode ein Wait zu setzen.
      /// </remarks>
      public static void FadeScreenIn()
      {
         Function.Call("DO_SCREEN_FADE_IN", new Parameter[0]);
      }

      /// <summary>
      /// Ersatz für die FadeScreenOut-Methode im .net script hook.
      /// </summary>
      /// <remarks>
      /// Leider kann keine Duration angegeben werden, weil es sonst zu einer
      /// Exception kommt (irgend etwas mit remote event nd Domain), darum
      /// dieser Ersatz. Ein einfacher workaround ist, hinter dem Aufruf der
      /// Methode ein Wait zu setzen.
      /// </remarks>
      public static void FadeScreenOut()
      {
         Function.Call("DO_SCREEN_FADE_OUT", new Parameter[0]);
      }

      /// <summary>
      /// The GetClosestPed method of the .net scripthook recognizes only
      /// peds who walk (or run?). This new method recognizes all peds in the
      /// collection no matter what they currently do. The Player.Character
      /// gets automatically excluded.
      /// </summary>
      /// <param name="playerCharacter">Inject <see cref="Player.Character"/> here.</param>
      /// <param name="pedsAround">A collection of peds around.</param>
      /// <returns>The closest ped (excluding the Player.Character).</returns>
      public static Ped GetClosestPed(ref Ped playerCharacter, ref Ped[] pedsAround)
      {
         var tempList = pedsAround.ToList();
         foreach(var ped in pedsAround)
         {
            if(ReferenceEquals(playerCharacter, ped))
               tempList.Remove(ped);
         }
         if(tempList.Count == 0)
            return null;
         var closestPed = tempList[0];
         foreach(var ped in tempList)
         {
            if(playerCharacter.Position.DistanceTo(ped.Position)
               < playerCharacter.Position.DistanceTo(closestPed.Position))
               closestPed = ped;
         }
         return closestPed;
      }

      /// <summary>
      /// Calls World.GetPeds(...) and returns an empty Ped array in case of
      /// a NonExistingObjectException.
      /// </summary>
      /// <param name="position"></param>
      /// <param name="radius">  </param>
      /// <returns>Peds inside a given radius at a given position.</returns>
      public static Ped[] GetPedsSafe(Vector3 position, float radius)
      {
         lock(SyncLock)
         {
            Ped[] pedsAround;
            try
            {
               pedsAround = World.GetPeds(position, radius);
            }
            catch(NonExistingObjectException)
            {
               DisplayText(SCRIPT_ENGINE_FAILURE_MESSAGE, 4000);
               return new Ped[] {};
            }
            return pedsAround;
         }
      }

      /// <summary>
      /// Gets a random integer number build with the
      /// RNGCryptoServiceProvider.
      /// </summary>
      /// <param name="min">The minimum.</param>
      /// <param name="max">The maximum.</param>
      /// <returns>a random integer number</returns>
      public static int GetRandomIntegerNumber(int min, int max)
      {
         lock(SyncLock)
         {
            if(max == 0)
               return 0;
            rnd = new RNGCryptoServiceProvider();
            var bytes = new byte[4];
            rnd.GetNonZeroBytes(bytes);
            rnd.Dispose();
            var randomInt = BitConverter.ToInt32(bytes, 0);
            return (Math.Abs(randomInt % (max + 1)) + min);
         }
      }

      /// <summary>
      /// Calls World.GetVehicles(...) and returns an empty Vehicle array in
      /// case of a NonExistingObjectException.
      /// </summary>
      /// <param name="position"></param>
      /// <param name="radius">  </param>
      /// <returns>
      /// Vehicles inside a given radius at a given position.
      /// </returns>
      public static Vehicle[] GetVehiclesSafe(Vector3 position, float radius)
      {
         lock(SyncLock)
         {
            Vehicle[] vehiclesAround;
            try
            {
               vehiclesAround = World.GetVehicles(position, radius);
            }
            catch(NonExistingObjectException)
            {
               DisplayText(SCRIPT_ENGINE_FAILURE_MESSAGE, 4000);
               return new Vehicle[] {};
            }
            return vehiclesAround;
         }
      }

      /// <summary>
      /// Calls World.GetVehicles(...) and returns an empty Vehicle array in
      /// case of a NonExistingObjectException.
      /// </summary>
      /// <param name="position"></param>
      /// <param name="radius">  </param>
      /// <param name="model">   </param>
      /// <returns>
      /// Vehicles inside a given radius at a given position.
      /// </returns>
      public static Vehicle[] GetVehiclesSafe(Vector3 position, float radius, Model model)
      {
         lock(SyncLock)
         {
            Vehicle[] vehiclesAround;
            try
            {
               vehiclesAround = World.GetVehicles(position, radius, model);
            }
            catch(NonExistingObjectException)
            {
               DisplayText(SCRIPT_ENGINE_FAILURE_MESSAGE, 4000);
               return new Vehicle[] {};
            }
            return vehiclesAround;
         }
      }

      /// <summary>
      /// Moves the <paramref name="ped"/> to the adjacent seat of the
      /// vehicle he is currently in.
      /// </summary>
      /// <remarks>
      /// Does nothing in case the vehicle has no adjacent seat like a bus
      /// or a bike. In case the vehicle is a bike the ped gets warped onto
      /// the driver seat.
      /// </remarks>
      /// <param name="ped">The ped.</param>
      public static void MoveToAdjacentSeat(Ped ped)
      {
         lock(SyncLock)
         {
            if(!ped.CurrentVehicle.Model.isBike)
               Function.Call("TASK_SHUFFLE_TO_NEXT_CAR_SEAT", new Parameter[]
                                                              {
                                                                 ped,
                                                                 ped.CurrentVehicle
                                                              });
            else
               ped.WarpIntoVehicle(ped.CurrentVehicle, VehicleSeat.Driver);
         }
      }

      /// <summary>
      /// Checks whether a ped has been created.
      /// </summary>
      /// <remarks>
      /// <c>World.Create</c> is unreliable. Always check the object
      /// creation!
      /// </remarks>
      public static bool PedExists(Ped ped)
      {
         lock(SyncLock)
         {
            return ped != null && Game.Exists(ped);
         }
      }

      /// <summary>
      /// Determines whether the player character is sitting on the driver
      /// seat.
      /// </summary>
      /// <param name="player">The player.</param>
      /// <param name="currentVehicle">The current vehicle.</param>
      /// <returns>
      /// <c>true</c> if the ped on the driver seat equals the player
      /// character, otherwise <c>false</c>.
      /// </returns>
      public static bool PlayerIsStittingOnDriversSeat(Ped player, Vehicle currentVehicle)
      {
         lock(SyncLock)
         {
            if(!VehicleExists(currentVehicle))
               return false;
            var ped = currentVehicle.GetPedOnSeat(VehicleSeat.Driver);
            return ReferenceEquals(player, ped);
         }
      }

      /// <summary>
      /// Rounds to a given factor.
      /// </summary>
      /// <param name="number">Any number.</param>
      /// <param name="factor">The factor that is to be used.</param>
      /// <returns>A value that's the closest integer factor.</returns>
      /// <remarks>
      /// Examples (if factor 20 is used): 46 becomes 40, 93 becomes 100, 74
      /// becomes 80, 117 becomes 120.
      /// </remarks>
      public static short RoundToFactor(short number, ushort factor)
      {
         lock(SyncLock)
         {
            var rest = number % factor;
            var numberWithoutRest = number - rest;
            int add;
            if(rest < 10)
               add = 0;
            else
               add = number - (numberWithoutRest - factor) > numberWithoutRest + factor - number ? factor : 0;
            return (short) (numberWithoutRest + add);
         }
      }

      /// <summary>
      /// Checks whether a vehicle has been created.
      /// </summary>
      /// <remarks>
      /// <c>World.Create</c> is unreliable. Always check the object
      /// creation!
      /// </remarks>
      public static bool VehicleExists(Vehicle vehicle)
      {
         lock(SyncLock)
         {
            return vehicle != null && Game.Exists(vehicle);
         }
      }
   }
}