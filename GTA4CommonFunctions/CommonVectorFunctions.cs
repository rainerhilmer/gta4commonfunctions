﻿using System;
// note: change the reference for test runs.
//using Cyron43.GtaIV.Common.FakesForTests;
using GTA;
using Object = System.Object;

namespace Cyron43.GtaIV.Common
{
   /// <summary>
   /// Provides vector functions the .net script hook lacks of. 
   /// </summary>
   /// <remarks>A heading starts north (0°) and increases counterclockwise.</remarks>
   public class CommonVectorFunctions
   {
      private static readonly Object SyncLock = new Object();

      /// <summary>
      /// The .net script hook suck again! Use this method instead.
      /// </summary>
      /// <param name="direction">The direction.</param>
      /// <returns>
      /// A heading in degrees that starts north (0°) and increases
      /// counterclockwise.
      /// </returns>
      public float DirectionToHeading(Vector3 direction)
      {
         //x-1=90; x1=270
         //y-1=180; y1=0
         lock(SyncLock)
         {
            return LineToHeading(new Vector3(0, 0, 0), direction);
         }
      }

      /// <summary>
      /// Calculates the heading of a line between two positions.
      /// </summary>
      /// <param name="position1">The position vector 1.</param>
      /// <param name="position2">The position vector 2.</param>
      /// <returns>
      /// A heading in degrees that starts north (0°) and increases
      /// counterclockwise.
      /// </returns>
      /// <remarks>This method ignores the Z-values.</remarks>
      public float LineToHeading(Vector3 position1, Vector3 position2)
      {
         // ArcTan((x2-x1)/(y2-y1))*(180/pi)
         lock(SyncLock)
         {
            var angle = (float) (Math.Atan2((position2.Y - position1.Y),
               (position2.X - position1.X))
                                 * 180 / Math.PI);
            // Beim Heading im .net script hook liegt 0° auf der Y-Achse.
            angle = angle - 90;
            if(angle < 0)
               angle += 360;
            return angle;
         }
      }

      /// <summary>
      /// Determines whether a target position is within a conical area. This
      /// area begins at the start position provided in the scan area, runs
      /// along the desired heading, spreads left and right about the heading
      /// tolerance and ends at the distance value. So the position must be
      /// within the given distance and within the given heading boundaries.
      /// </summary>
      /// <param name="position">The ped position.</param>
      /// <param name="area">    The area.</param>
      /// <returns>
      /// <c>true</c> if the target position is within the given scan area,
      /// otherwise <c>false</c>.
      /// </returns>
      /// <example>
      /// <code>
      /// <![CDATA[
      ///       private Ped GetPedInFront(ref List<Ped> pedsAround)
      ///       {
      ///          var tempList = new List<Ped>();
      ///          var area = new ScanArea
      ///                     {
      ///                        GeneralScanAreaDirection = Player.Character.Direction,
      ///                        MaximumDistance = MAX_SCAN_DISTANCE,
      ///                        HeadingTolerance = 10,
      ///                        StartPosition = Player.Character.Position
      ///                     };
      /// 
      ///          var commonVectorFunctions = new CommonVectorFunctions();
      /// 
      ///          foreach(var ped in pedsAround)
      ///          {
      ///             if(!commonVectorFunctions.TargetPositionIsWithinArea(ped.Position, area))
      ///                continue;
      ///             if(!ReferenceEquals(Player.Character, ped))
      ///                tempList.Add(ped);
      ///          }
      ///          return tempList.Count > 0 ? tempList[0] : null;
      ///       }
      /// ]]>
      /// </code>
      /// </example>
      /// <exception cref="ArgumentOutOfRangeException">
      /// if either the desired heading, the actual heading or the heading
      /// tolerance is out of range.
      /// </exception>
      public bool TargetPositionIsWithinArea(Vector3 position, ScanArea area)
      {
         lock(SyncLock)
         {
            var desiredHeading = DirectionToHeading(area.GeneralScanAreaDirection);
            var actualHeading = LineToHeading(area.StartPosition, position);
            bool distanceCondition = position.DistanceTo2D(area.StartPosition) <= area.MaximumDistance;
            bool headingCondition = IsWithinHeadingTolerance(desiredHeading, actualHeading,
               area.HeadingTolerance);
            return distanceCondition && headingCondition;
         }
      }

      private static bool IsWithinHeadingTolerance(
         float desiredHeading, float actualHeading, float toleranceDegrees)
      {
         if(desiredHeading < 0 || desiredHeading > 360)
            throw new ArgumentOutOfRangeException("desiredHeading");
         if(actualHeading < 0 || actualHeading > 360)
            throw new ArgumentOutOfRangeException("desiredHeading");
         if(toleranceDegrees < 0 || toleranceDegrees > 180)
            throw new ArgumentOutOfRangeException("toleranceDegrees");
         float minHeading = desiredHeading - toleranceDegrees;
         float maxHeading = desiredHeading + toleranceDegrees;
         if(maxHeading > 360)
            maxHeading -= 360;
         return actualHeading >= minHeading && actualHeading <= maxHeading;
      }
   }
}