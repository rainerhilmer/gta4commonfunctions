﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;

namespace Cyron43.GtaIV.Common
{
   /// <summary>
   /// Provides a central handling for XML Configurations for my GTA IV mods.
   /// This is a thread safe singleton class which registers each calling
   /// assembly (Mod) for the individual ConfigurationContainer provision.
   /// This class is not inheritable.
   /// </summary>
   public sealed class ConfigurationProvider
   {
      private static readonly Dictionary<string, string> PathValuesForAssemblies = new Dictionary<string, string>();
      private static readonly Object SyncLock = new Object();
      private static ConfigurationProvider myself;

      private ConfigurationProvider()
      {
      }

      /// <summary>
      /// Creates or gets the singleton instance instance of this class.
      /// </summary>
      /// <param name="assemblyData">
      /// An instance of a class which implements <see cref="IModIdentity"/>.
      /// </param>
      /// <returns>The instance of the <see cref="ConfigurationProvider"/></returns>
      public static ConfigurationProvider CreateOrGetThisFor(IModIdentity assemblyData)
      {
         lock(SyncLock)
         {
            if(myself == null)
               myself = new ConfigurationProvider();
            AddFullPathForAssemblyIfNotAlreadySet(assemblyData.AssemblyName, assemblyData.FullPath);
            return myself;
         }
      }

      /// <summary>
      /// Saves a ConfigurationContainer which implements
      /// <see cref="IConfigurationContainer"/>.
      /// </summary>
      /// <param name="config">  </param>
      /// <param name="fullPath">
      /// The full path including filename and extension.
      /// </param>
      /// <typeparam name="T">
      /// A ConfigurationContainer which implements
      /// <see cref="IConfigurationContainer"/>
      /// </typeparam>
      public static void SaveConfig<T>(T config, string fullPath)
         where T : IConfigurationContainer
      {
         new XmlIo(fullPath).Save(config);
      }

      /// <summary>
      /// Gets the configuration for the assembly with the given name.
      /// </summary>
      /// <typeparam name="T">
      /// The ConfigurationContainer which must implement <see
      /// cref="IConfigurationContainer"/>.
      /// </typeparam>
      /// <param name="assemblyData">        
      /// An class which implements <see cref="IModIdentity"/> and holds all
      /// necessary data.
      /// </param>
      /// <param name="typeOfError">         
      /// Returns the reason why the configuration file was not read. Returns
      /// <see cref="ErrorType.None"/> when the configuration file has been
      /// successfully read.
      /// </param>
      /// <param name="supressInGameMessage">
      /// Supresses the display of an in-game message in case of an error and
      /// throws exceptions instead. The main purpose of this parameter are
      /// unit tests which cannot run if any call to the ScriptHookDotnet is
      /// made. Also debugging is easier that way.
      /// </param>
      /// <returns>
      /// The specific ConfigurationContainer which must implement <see
      /// cref="IConfigurationContainer"/>. The content of the
      /// ConfigurationContainer will be empty in case of an error.
      /// </returns>
      public T GetConfigurationFor<T>(
         IModIdentity assemblyData,
         out ErrorType typeOfError,
         bool supressInGameMessage = false)
         where T : class, IConfigurationContainer
      {
         lock(SyncLock)
         {
            T config;
            typeOfError = ErrorType.Undefined;
            try
            {
               config = new XmlIo(PathValuesForAssemblies[assemblyData.AssemblyName]).Load<T>();
            }
            catch(FileNotFoundException)
            {
               if(!supressInGameMessage)
                  CommonFunctions.DisplayText(
                     "~r~"
                     + assemblyData.AssemblyName
                     + " configuration file not found!"
                     + " Please read the guide for troubleshooting.~w~", 10000);
               typeOfError = ErrorType.FileNotFound;
               return default(T);
            }
            catch(KeyNotFoundException)
            {
               if(!supressInGameMessage)
                  CommonFunctions.DisplayText(
                     "~r~ The mod has not registered at the configuration"
                     + " provider. Please report this to the developer.~w~", 10000);
               typeOfError = ErrorType.KeyNotFound;
               return default(T);
            }
            catch(SerializationException)
            {
               if(!supressInGameMessage)
                  CommonFunctions.DisplayText(
                     "~r~Malformed "
                     + assemblyData.AssemblyName
                     + " configuration file found!"
                     + " Please read the guide for troubleshooting.~w~", 10000);
               typeOfError = ErrorType.SerializationException;
               return default(T);
            }
            if(config == null)
            {
               typeOfError = ErrorType.Undefined;
               return default(T);
            }
            if(!ConfigVersionMatches(assemblyData.Version, ref config))
            {               
               if(!supressInGameMessage)
                  CommonFunctions.DisplayText(
                     "~r~Wrong "
                     + assemblyData.AssemblyName
                     + " configuration file version found!"
                     + " Please read the guide for troubleshooting.~w~", 10000);
               typeOfError = ErrorType.WrongVersion;
               return default(T);
            }            
            typeOfError = ErrorType.None;
            return config;
         }
      }

      /// <summary>
      /// Clears the dictionary which holds the path information for each
      /// registered assembly. The purpose of this method is solely for unit
      /// tests.
      /// </summary>
      internal static void ClearDictionary()
      {
         PathValuesForAssemblies.Clear();
      }

      private static void AddFullPathForAssemblyIfNotAlreadySet(string assemblyName, string fullPathConfigFile)
      {
         lock(SyncLock)
         {
            if(PathValuesForAssemblies.ContainsKey(assemblyName))
               return;
            PathValuesForAssemblies.Add(assemblyName, fullPathConfigFile);
         }
      }

      private static bool ConfigVersionMatches<T>(int majorVersion, ref T config)
         where T : IConfigurationContainer
      {
         return config.Version == majorVersion;
      }
   }
}