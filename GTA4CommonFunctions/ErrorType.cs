﻿namespace Cyron43.GtaIV.Common
{
   /// <summary>
   /// Specifies the type of error that happened (<c>None</c> when no error occured)
   /// </summary>
   public enum ErrorType
   {
      /// <summary>
      /// This type should never occur but if it does it indicates it has not
      /// been cared for where it should be.
      /// </summary>
      Undefined = 0,
      
      /// <summary>
      /// No error occured.
      /// </summary>
      None,

      /// <summary>
      /// Is set when the requested configuration file could not be found.
      /// </summary>
      FileNotFound,

      /// <summary>
      /// Is set when the calling assembly has not registered before the call.
      /// </summary>
      KeyNotFound,

      /// <summary>
      /// Is set in case of any format error inside the XML configzration file.
      /// </summary>
      SerializationException,

      /// <summary>
      /// Is set when the version number read from the configuration file doe
      /// not match the majr version number of the calling assembly.
      /// </summary>
      WrongVersion
   }
}