﻿namespace Cyron43.GtaIV.Common
{
   /// <summary>
   /// All ConfigurationContainers must implement this interface.
   /// </summary>
   public interface IConfigurationContainer
   {
      /// <summary>
      /// Gets or sets the version of the assembly connected with a specific 
      ///  <c>ConfigurationContainer</c> which implements this interface.
      /// </summary>
      /// <value>The major version.</value>
      /// <remarks>
      /// It is recommended to use the following code to retrieve the major
      /// version number: <example>
      /// <code>
      /// <![CDATA[
      /// var majorVersion = Assembly.GetExecutingAssembly().GetName().Version.Major;
      /// ]]>
      /// </code></example>
      /// </remarks>
      int Version { get; set; }
   }
}
