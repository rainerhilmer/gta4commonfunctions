﻿namespace Cyron43.GtaIV.Common
{
   /// <summary>
   /// Defines the common parts of a location.
   /// </summary>
   public interface ILocation
   {
      /// <summary>
      /// The ID of a location.
      /// </summary>
      int Id { get; set; }

      /// <summary>
      /// Gets or sets the X-value of the position.
      /// </summary>
      float X { get; set; }
      
      /// <summary>
      /// Gets or sets the Y-value of the position.
      /// </summary>
      float Y { get; set; }
      
      /// <summary>
      /// Gets or sets the Z-value of the position.
      /// </summary>
      float Z { get; set; }
      
      /// <summary>
      ///    z.B. "Uno building", "Hotel Majestic", ...
      /// </summary>
      string TargetName { get; set; }
   }
}
