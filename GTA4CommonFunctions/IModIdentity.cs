﻿namespace Cyron43.GtaIV.Common
{
   /// <summary>
   /// Contract for various assembly information which are needed by the
   ///  <see cref="ConfigurationProvider"/>.
   /// </summary>
   /// <remarks>
   /// It is recommended to call the data via a static provoder class.
   /// Example:
   /// </remarks>
   /// <example>
   /// <code>
   /// <![CDATA[
   ///    public static class IdentityProvider
   ///    {
   ///       /* Das Interface als Rückgabetyp verhindert,
   ///        * dass die inneren Properties ausserhalb gesetzt werden könnnen. */
   ///       public static IModIdentity Identity
   ///       {
   ///          get
   ///          {
   ///             return new ModIdentity
   ///                    {
   ///                       AssemblyName = Assembly.GetExecutingAssembly().GetName().Name,
   ///                       FullPath = @"a:\Test.xml",
   ///                       Version = Assembly.GetExecutingAssembly().GetName().Version.Major
   ///                    };
   ///          }
   ///       }
   ///    }
   /// ]]>
   /// </code>
   /// </example>
   public interface IModIdentity
   {
      /// <summary>
      /// Gets the name of the assembly.
      /// </summary>
      /// <value>
      /// The name of the assembly.
      /// </value>
      /// <remarks>
      /// It is recommended to use the following code to retrieve the
      /// assembly name: <example>
      /// <code>
      /// <![CDATA[
      /// var assemblyName = Assembly.GetExecutingAssembly().GetName().Name
      /// ]]>
      /// </code></example>
      /// </remarks>
      string AssemblyName { get; }

      /// <summary>
      /// Gets the full path which includes the filename and the extension.
      /// </summary>
      /// <value>The full path.</value>
      /// <remarks>
      /// You can retrieve the path part via <see
      /// cref="CommonFunctions.FileRepositoryPath"/>.
      /// </remarks>
      string FullPath { get; }

      /// <summary>
      /// Gets the assembly version.
      /// </summary>
      /// <value>The version.</value>
      /// <remarks>
      /// It is recommended to use the following code to retrieve the major
      /// version number: <example>
      /// <code>
      /// <![CDATA[
      /// var majorVersion = Assembly.GetExecutingAssembly().GetName().Version.Major;
      /// ]]>
      /// </code></example>
      /// </remarks>
      int Version { get; }
   }
}