﻿using System.Windows.Forms;

namespace Cyron43.GtaIV.Common
{
   /// <summary>
   /// A container which holds all necessary data for a key press validation.
   /// </summary>
   public class KeyContainer
   {
#pragma warning disable 1591 // Disables nagging about missing XML comments.
      public bool Alt { get; set; }
      public bool Ctrl { get; set; }
      public Keys Key { get; set; }
      public string Name { get; set; }
      public bool Shift { get; set; }
#pragma warning restore 1591
   }
}