﻿using System.Collections.Generic;
using GTA;

namespace Cyron43.GtaIV.Common
{
   /// <summary>
   /// Provides functionality which is common across my mods.
   /// </summary>
   /// <remarks>All methods in this class are thread safe.</remarks>
   public static class KeyHandlingCommons
   {
      private static readonly object SyncLock = new object();

      /// <summary>
      /// Gets the key container with the specified name from a list of key
      /// containers.
      /// </summary>
      /// <param name="keys">   
      /// A List&lt;<see cref="KeyContainer"/>&gt; instance.
      /// </param>
      /// <param name="keyName">
      /// The name of the <see cref="KeyContainer"/> to get.
      /// </param>
      /// <returns>
      /// A <see cref="KeyContainer"/> that matches the given name or
      /// <c>null</c> if the key container could not be found.
      /// </returns>
      /// <remarks>
      /// Displays an error message in the game if the key container could
      /// not be found.
      /// </remarks>
      /// <example>
      /// <code>
      /// <![CDATA[
      /// if(!KeyHandlingCommons.KeyIs(KeyHandlingCommons.GetKeyContainer(_core.Configuration.Keys, "SlowDown"), e))
      ///    return;
      /// ]]>
      /// </code>
      /// </example>
      public static KeyContainer GetKeyContainer(List<KeyContainer> keys, string keyName)
      {
         lock(SyncLock)
         {
            var key = keys.Find(keyContainer => keyContainer.Name == keyName);
            if(key == null)
               CommonFunctions.DisplayText("Error: Key configuration not found! Check KeyContainer.Name", 5000);
            return key;
         }
      }

      /// <summary>
      /// Determines whether a pressed keyboard key-(combination) matches a
      /// certain pattern which is defined in a given <see
      /// cref="KeyContainer"/>.
      /// </summary>
      /// <param name="keyContainer">
      /// <see cref="KeyContainer"/> that defines a certain
      /// key-(combination).
      /// </param>
      /// <param name="keyEventArgs">
      /// The <see cref="GTA.KeyEventArgs"/> instance containing the event
      /// data.
      /// </param>
      /// <remarks>Provide GTA.KeyEventArgs, not System.Windows.Forms.KeyEventArgs.</remarks>
      /// <returns>
      /// <c>true</c> if the pressed key-(combination) matches the definition
      /// given by the <see cref="KeyContainer"/>, otherwise <c>false</c>.
      /// </returns>
      /// <example>
      /// <code>
      /// <![CDATA[
      ///       private void AtPedMove(KeyEventArgs e)
      ///       {
      ///          var keyContainer = new KeyContainer
      ///                             {
      ///                                Alt = true,
      ///                                Ctrl = false,
      ///                                Key = Keys.J,
      ///                                Name = "MovePed",
      ///                                Shift = false
      ///                             };
      ///          if(KeyHandlingCommons.KeyIs(keyContainer, e))
      ///             _core.AskPedToMove();
      ///       }
      /// ]]>
      /// </code>
      /// </example>
      /// <example>
      /// <code>
      /// <![CDATA[
      /// if(!KeyHandlingCommons.KeyIs(KeyHandlingCommons.GetKeyContainer(_core.Configuration.Keys, "SlowDown"), e))
      ///    return;
      /// ]]>
      /// </code>
      /// </example>
      public static bool KeyIs(KeyContainer keyContainer, KeyEventArgs keyEventArgs)
      {
         lock(SyncLock)
         {
            return keyContainer.Alt == keyEventArgs.Alt
                   && keyContainer.Ctrl == keyEventArgs.Control
                   && keyContainer.Shift == keyEventArgs.Shift
                   && keyContainer.Key == keyEventArgs.Key;
         }
      }
   }
}
