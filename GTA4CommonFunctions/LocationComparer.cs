﻿using System.Collections.Generic;
using GTA;

namespace Cyron43.GtaIV.Common
{
   /// <summary>
   /// Provides a distance comparison of two locations that implement the
   /// <see cref="ILocation"/> interface.
   /// </summary>
   public class LocationComparer : Comparer<ILocation>
   {
      private readonly Vector3 _currentPosition;

      /// <summary>
      /// Initializes a new instance of the <see cref="LocationComparer"/>
      ///  class.
      /// </summary>
      /// <param name="currentPosition">
      /// A <see cref="Vector3"/> position that is used to compare the
      ///  distance of locations to.
      /// </param>
      public LocationComparer(Vector3 currentPosition)
      {
         _currentPosition = currentPosition;
      }

      /// <summary>
      /// Compares the flat (2D) distances of two locations relatively to a
      ///  given position (provided in the constructor of the
      ///  LocationComparer.
      /// </summary>
      /// <param name="x">The first location to compare.</param>
      /// <param name="y">The second location to compare.</param>
      /// <returns>
      /// - 1 if the distance of location x to the current position is less
      ///   than the distance of location y to the current position.
      /// 0 if both distances are equal.
      /// + 1 if the distance of location x to the current position is
      ///   greater than the distance of location y to the current position.
      /// </returns>
      /// <example>
      /// <code>
      /// <![CDATA[
      ///       private List<Location> Get30ClosestLocations(ref List<Location> locations, ref Vector3 currentPosition)
      ///       {
      ///          if(locations.Count < 31)
      ///             return locations;
      ///          locations.Sort(new LocationComparer(currentPosition).Compare);
      ///          var rangeEnd = locations.Count - 30;
      ///          locations.RemoveRange(30, rangeEnd);
      ///          return locations;
      ///       }
      /// ]]>
      /// </code>
      /// </example>
      public override int Compare(ILocation x, ILocation y)
      {
         if(x == null || y == null)
            return -1;
         var pos1 = new Vector3(x.X, x.Y, x.Z);
         var pos2 = new Vector3(y.X, y.Y, y.Z);
         return pos1.DistanceTo2D(_currentPosition)
            .CompareTo(pos2.DistanceTo2D(_currentPosition));
      }
   }
}