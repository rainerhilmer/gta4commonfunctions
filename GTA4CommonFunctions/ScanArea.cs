﻿// note: change the reference for test runs.
//using Cyron43.GtaIV.Common.FakesForTests;
using GTA;

namespace Cyron43.GtaIV.Common
{
   /// <summary>
   /// A container that holds all data necessary to feed the
   /// <see cref="CommonVectorFunctions.TargetPositionIsWithinArea"/> method.
   /// </summary>
   public class ScanArea
   {
      /// <summary>
      /// Gets or sets the general direction of the scan area. This is
      /// internally recalculated to a heading which used to determine
      /// whether i.e. a ped is within the scan area.
      /// </summary>
      /// <value>The desired heading.</value>
      public Vector3 GeneralScanAreaDirection { get; set; }

      /// <summary>
      /// Gets or sets the heading tolerance in degrees. This value adds left
      /// and right to the desired heading. The result is a conical area in
      /// which the target position has to be.
      /// </summary>
      /// <value>The heading tolerance.</value>
      public float HeadingTolerance { get; set; }

      /// <summary>
      /// Gets or sets the maximum distance allowed for the target position
      /// to be considered as the desired one.
      /// </summary>
      /// <value>The distance.</value>
      public float MaximumDistance { get; set; }
      
      /// <summary>
      /// Gets or sets the start position where the conical area begins.
      /// Usually that's the player character position.
      /// </summary>
      /// <value>The start position.</value>
      public Vector3 StartPosition { get; set; }
   }
}
