﻿using Cyron43.GtaIV.Common;

namespace SomeTestAssembly
{
   public class ConfigurationContainer : IConfigurationContainer
   {
      public int SomeNumber { get; set; }
      public int Version { get; set; }
   }
}