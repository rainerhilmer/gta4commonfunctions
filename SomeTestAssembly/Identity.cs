﻿using System.Reflection;
using Cyron43.GtaIV.Common;

namespace SomeTestAssembly
{
   public class  Identity : IModIdentity
   {
      public string AssemblyName { get { return Assembly.GetExecutingAssembly().GetName().Name; } }
      public string FullPath { get { return @"a:\SomeTestAssemblyConfig.xml"; } }
      public int Version { get { return Assembly.GetExecutingAssembly().GetName().Version.Major; } }
   }
}
