﻿using System.Reflection;
using Cyron43.GtaIV.Common;

namespace SomeTestAssembly
{
   public static class IdentityProvider
   {
      public static IModIdentity Identity
      {
         get
         {
            return new ModIdentity
            {
               AssemblyName = Assembly.GetExecutingAssembly().GetName().Name,
               FullPath = @"a:\SomeTestAssemblyConfig.xml",
               Version = Assembly.GetExecutingAssembly().GetName().Version.Major
            };
         }
      }
   }
}
