﻿using Cyron43.GtaIV.Common;

namespace SomeTestAssembly
{
   public class  ModIdentity : IModIdentity
   {
      public string AssemblyName { get; set; }
      public string FullPath { get; set; }
      public int Version { get; set; }
   }
}
