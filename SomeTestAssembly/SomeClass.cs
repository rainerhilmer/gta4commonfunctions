﻿using Cyron43.GtaIV.Common;

namespace SomeTestAssembly
{
   public class SomeClass
   {
      public ConfigurationContainer Configuration { get; private set; }

      public ErrorType ReadMyConfig()
      {
         var configProvider = ConfigurationProvider.CreateOrGetThisFor(IdentityProvider.Identity);
         ErrorType configIsValid;
         Configuration = configProvider
            .GetConfigurationFor<ConfigurationContainer>(
            IdentityProvider.Identity,
            out configIsValid,
            supressInGameMessage: true);
         return configIsValid;
      }
   }
}
